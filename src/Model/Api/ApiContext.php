<?php
namespace App\Model\Api;

use Curl\Curl;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ApiContext
{
    /** @var Curl */
    private $curl;

    /**
     * @var ContainerInterface
     */
    private $container;

    /** @var string */
    private $apiToken;

    /** @var string */
    private $apiUrl;

    const ENDPOINT_PING = '/ping';

    const METHOD_POST = 1;
    const METHOD_GET = 2;
    const METHOD_HEAD = 3;
    const METHOD_PUT = 4;
    const METHOD_DELETE = 5;
    const METHOD_PATCH = 6;

    /**
     * Context constructor.
     * @param ContainerInterface $container
     * @throws \ErrorException
     */
    public function __construct(
        ContainerInterface $container
    ) {
        $this->container = $container;

        $this->curl = new Curl();
        $this->apiUrl = $this
            ->container
            ->getParameter('central')['api']['url'];
        $this->apiToken = $this
            ->container
            ->getParameter('central')['api']['token'];
    }

    /**
     * @return mixed
     * @throws ApiException
     */
    public function makePing() {
        return $this->makeQuery(self::ENDPOINT_PING, self::METHOD_GET);
    }

    /**
     * @param string $endpoint
     * @param int $method
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    private function makeQuery(string $endpoint, int $method, $data = []) {
        $url = $this->apiUrl.$endpoint;
        $data = array_merge($data, ['token' => $this->apiToken]);

        switch ($method) {
            case self::METHOD_GET:
                $this->curl->get($url, $data);
                break;
            case self::METHOD_DELETE:
                $this->curl->delete($url, $data);
                break;
            case self::METHOD_HEAD:
                $this->curl->head($url, $data);
                break;
            case self::METHOD_PATCH:
                $this->curl->patch($url, $data);
                break;
            case self::METHOD_POST:
                $this->curl->post($url, $data);
                break;
            case self::METHOD_PUT:
                $this->curl->put($url, $data);
                break;
        }

        if ($this->curl->error) {
            throw new ApiException(
                $this->curl->errorCode . ': ' . $this->curl->errorMessage);
        } else {
            return $this->curl->response;
        }
    }
}