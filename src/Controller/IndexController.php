<?php

namespace App\Controller;

use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class IndexController extends Controller
{
    /**
     * @Route("/", name="app_home")
     */
    public function indexAction(ApiContext $context)
    {
        return $this->render('index.html.twig', []);
    }

    /**
     * @Route("/social-auth-back", name="app_social_auth_back")
     */
    public function socialAuthBackAction(ApiContext $context)
    {
        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $user = json_decode($s, true);
        //$user['network'] - соц. сеть, через которую авторизовался пользователь
        //$user['identity'] - уникальная строка определяющая конкретного пользователя соц. сети
        //$user['first_name'] - имя пользователя
        //$user['last_name'] - фамилия пользователя

        dump($user);

        return $this->render('index.html.twig', []);
    }

    /**
     * @Route("/make-ping", name="app_make_ping")
     */
    public function makePingAction(ApiContext $context)
    {
        try {
            return new Response(var_export($context->makePing(), true));
        } catch (ApiException $e) {
            return new Response($e->getMessage());
        }
    }

    /**
     * @Route("/auth", name="auth")
     */
    public function authAction(UserRepository $userRepository)
    {
        $user = $userRepository->find(1);
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $this
            ->container
            ->get('security.token_storage')
            ->setToken($token);
        $this
            ->container
            ->get('session')
            ->set('_security_main', serialize($token));
        return new Response('Вы авторизованы');
    }

    /**
     * @Route("/qwe", name="qwe")
     */
    public function qweAction()
    {
        return new Response('1231312');
    }
}
